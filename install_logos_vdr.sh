#!/bin/bash -e
#set -x

#CONFIG
SOURCEDIR=./logos #logo source directory / путь к логотипам с прозрачными фонами
OUTPUTDIR=/usr/share/vdr/plugins/skinnopacity/logos #nopacity logos directory / путь к папке назначения (папка логотипов скина nopacity)
BACKGROUND=./backgrounds/bg3.png #choose your background / путь к файлу фона логотипа
FOREGROUND=./backgrounds/fg4.png #choose your foreground / путь к файлу блика логотипа
CHANCFG=/var/lib/vdr/channels.conf #path to you channels.conf file / путь к файлу списка каналов VDR
COLOR="white" #font color on the autologos / цвет шрифта на сгенерированных логотипах


#MAIN
if [ "$(grep -l ''  $OUTPUTDIR/*.png)" ] ; then
    echo "Удалить все PNG файлы в папке назначения ? (y/n)"
    /bin/rm -I $OUTPUTDIR/*.png 2> /dev/null
fi

./utils/make_duplicates.sh

IFS=$'\n'
FILES=$(find $SOURCEDIR -name *.png | sort)
sedstring="s!$SOURCEDIR!$OUTPUTDIR!g"

[ ! -d $SOURCEDIR ] && (echo "ERR: $SOURCEDIR not found"; exit 1);
[ ! -f $FOREGROUND ] && (echo "ERR: $FOREGROUND not found"; exit 1);
[ ! -f $BACKGROUND ] && (echo "ERR: $BACKGROUND not found"; exit 1);

echo "Convert new logos to format $(basename $OUTPUTDIR)"
for file in $FILES; do
  targetdir=`dirname $file | sed -e $sedstring`
  lcase_file=$(tr 'A-Z' 'a-z' <<< $(basename $file))
  targetfile="$OUTPUTDIR/$lcase_file"
#  targetfile="$targetdir/$(basename $file)"
  if [ ! -f $targetfile ] ; then
    echo $targetfile
    [ ! -d $targetdir ]  && mkdir -p $targetdir
    convert +dither -background 'transparent' -resize '220x164' -extent '268x200' -gravity 'center' "$file" png:- 2> /dev/null | \
      composite - $BACKGROUND png:- 2> /dev/null | \
      composite -compose screen -blend 50x100 $FOREGROUND - "$targetfile" 2> /dev/null
  fi
done

yesno ()
{
  while true; do
  printf "$1 (Y/n) ? "
  read ans
  case X"$ans" in
   X|Xy|XY) return 0;;
   Xn|XN) return 1;;
  esac
  done
}

./utils/del_duplicates.sh

ANS= yesno "Сгенерировать отсутствующие логотипы ?"

if [ $ANS=0 ]; then


    echo "Генерация отсутствующих логотипов"
    echo "=================================="

    [ ! -d $OUTPUTDIR/tmp ]  && mkdir -p $OUTPUTDIR/tmp

    FILES=$(sed -e 's/;.*//g' <<< sed -e '/^:/d' $CHANCFG)
    for file in $FILES; do

        lcase_file=$(tr 'A-Z' 'a-z' <<< $file)
        targetfile="$OUTPUTDIR/$lcase_file.png"
        tmpfile="$OUTPUTDIR/tmp/$lcase_file.png"
        filename=$(sed 's/[ \t]*$//;s/ /\\n/g' <<< $file)


        if [[ ! -f $targetfile && ! "$lcase_file" == */* ]] ; then

	    echo $targetfile
            montage \
              -size 268x200 \
              -background none \
              -gravity center \
              -fill $COLOR \
              -font Bookman-Demi \
               label:$filename +set label \
              -shadow \
              -background transparent \
              -geometry +5+5 \
	      $tmpfile
	
	    convert +dither -background 'transparent' -resize '220x164' -extent '268x200' -gravity 'center' "$tmpfile" png:- 2> /dev/null | \
             composite - $BACKGROUND png:- 2> /dev/null | \
             composite -compose screen -blend 50x100 $FOREGROUND - "$targetfile" 2> /dev/null
	fi
    done
    [ "$(ls -A $OUTPUTDIR/tmp)" ] && /bin/rm $OUTPUTDIR/tmp/*.png
    /bin/rmdir $OUTPUTDIR/tmp
fi 



exit 0

