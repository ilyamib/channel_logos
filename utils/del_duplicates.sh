#!/bin/bash -e
#set -x

#CONFIG
INDIR=./logos
OUTDIR=./logos


IFS=$'\n'
FILES=$(cat list_of_duplicates)

for file in $FILES; do

    infile=$(cut -d = -f 1 <<< $file)
    outfile=$(cut -d = -f 2 <<< $file)

    /bin/rm $OUTDIR/$outfile.png

    echo $OUTDIR/$outfile.png - удален

done
